import Combine

public class Navigator: ObservableObject {
    private let parent: Navigator?
    
    @Published
    internal var currentRoute: String?
 
    public init() {
        self.parent = nil
    }
    
    public init(parent: Navigator) {
        self.parent = parent
    }
    
    public func pop() {
        self.parent?.currentRoute = nil
    }
    
    public func replace(withRoute route: String) {
        self.parent?.currentRoute = route
    }
    
    public func push(route: String) {
        self.currentRoute = route
    }
    
    public func popToRoot() {
        if let parent: Navigator = self.parent {
            parent.popToRoot()
        }
        self.pop()
    }
}
