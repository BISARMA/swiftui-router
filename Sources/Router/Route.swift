import SwiftUI

public struct Route: View, Identifiable {
    public let id: String
    
    private let destination: AnyView
    private let children: [Route]
    
    @EnvironmentObject
    private var navigator: Navigator
    
    public var body: some View {
        ZStack {
            ForEach(self.children) { (child: Route) in
                NavigationLink(
                    tag: child.id,
                    selection: self.$navigator.currentRoute
                ) {
                    child.environmentObject(Navigator(parent: self.navigator))
                } label: {
                    EmptyView()
                }
            }
            self.destination
        }
    }
    
    public init<Destination: View>(
        id: String,
        destination: Destination,
        @RouteBuilder children: () -> [Route]
    ) {
        self.id = id
        self.destination = AnyView(destination)
        self.children = children()
    }
    
    public init<Destination: View>(
        id: String,
        @ViewBuilder destination: () -> Destination,
        @RouteBuilder children: () -> [Route]
    ) {
        self.id = id
        self.destination = AnyView(destination())
        self.children = children()
    }
}
