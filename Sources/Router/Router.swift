import SwiftUI

public struct Router: View {
    private let content: Route
    
    public var body: some View {
        NavigationView {
            self.content.environmentObject(Navigator())
        }
        .navigationViewStyle(.stack)
    }
    
    public init(content: () -> Route) {
        self.content = content()
    }
}
