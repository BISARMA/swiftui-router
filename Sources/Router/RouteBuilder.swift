import SwiftUI

@resultBuilder
public class RouteBuilder {
    public static func buildBlock(_ components: Route...) -> [Route] {
        return components
    }
    
    public static func buildArray(_ components: [Route]) -> [Route] {
        return components
    }
    
    public static func buildOptional(_ component: Route?) -> [Route] {
        guard let component else { return [] }
        return [component]
    }
    
    public static func buildEither(first component: Route) -> [Route] {
        return [component]
    }
    
    public static func buildEither(second component: Route) -> [Route] {
        return [component]
    }
}
