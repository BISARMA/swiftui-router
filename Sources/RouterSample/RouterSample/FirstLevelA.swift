import SwiftUI
import Router

internal struct FirstLevelA: View {
    @EnvironmentObject
    private var navigator: Navigator
    
    internal var body: some View {
        VStack {
            Text("FirstLevelA")
            Button("Go to Root/FirstLevelA/SecondLevel") {
                self.navigator.push(route: "SecondLevel")
            }
        }
        .padding()
    }
}
