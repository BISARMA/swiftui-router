import SwiftUI
import Router

internal struct SecondLevel: View {
    @EnvironmentObject
    private var navigator: Navigator
    
    internal var body: some View {
        VStack {
            Text("SecondLevel")
            Button("Go to Root") {
                self.navigator.popToRoot()
            }
        }
        .padding()
    }
}
