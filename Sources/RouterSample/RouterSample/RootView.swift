import SwiftUI
import Router

internal struct RootView: View {
    @EnvironmentObject
    private var navigator: Navigator
    
    internal var body: some View {
        VStack {
            Text("RootView")
            Button("Go to Root/FirstLevelA") {
                self.navigator.push(route: "FirstLevelA")
            }
            Button("Go to Root/FirstLevelB") {
                self.navigator.push(route: "FirstLevelB")
            }
        }
        .padding()
    }
}
