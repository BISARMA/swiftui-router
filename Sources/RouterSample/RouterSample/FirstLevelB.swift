import SwiftUI
import Router

internal struct FirstLevelB: View {
    @EnvironmentObject
    private var navigator: Navigator
    
    internal var body: some View {
        VStack {
            Text("FirstLevelB")
            Button("Go to Root/FirstLevelA") {
                self.navigator.replace(withRoute: "FirstLevelA")
            }
            Button("Go to Root") {
                self.navigator.pop()
            }
        }
        .padding()
    }
}
