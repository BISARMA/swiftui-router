import SwiftUI
import Router

@main
internal struct RouterSampleApp: App {
    internal var body: some Scene {
        WindowGroup {
            Router {
                Route(id: "", destination: RootView()) {
                    Route(id: "FirstLevelA", destination: FirstLevelA()) {
                        Route(id: "SecondLevel", destination: SecondLevel()) {
                        }
                    }
                    Route(id: "FirstLevelB", destination: FirstLevelB()) {
                    }
                }
            }
        }
    }
}
